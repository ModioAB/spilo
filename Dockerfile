ARG IMAGE_BUILD_FROM
FROM $IMAGE_BUILD_FROM

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown

LABEL "se.modio.ci.url"=$URL  "se.modio.ci.branch"=$BRANCH  "se.modio.ci.commit"=$COMMIT  "se.modio.ci.host"=$HOST  "se.modio.ci.date"=$DATE

# Cron is used in the container to schedule backups. We don't need to run apt-get update.
RUN truncate -s0 /etc/crontab
